// React
import React, { useEffect, useState } from 'react'
// Style
import './Comic.scss'
// Material UI
import Rating from '@material-ui/lab/Rating';
import { Box, Button, CircularProgress } from '@material-ui/core';

const Comic = () => {
    useEffect(() => {
        getComicData()
    }, [])

    const [value, setValue] = useState()
    const [Comic, setComic] = useState({})
    const [loading, setLoading] = useState(true)

    /**
     * Method for generate a new comic with a random number
     */
    const getComicData = () => {
        // Random for send it to the url and generate a new comic
        const random = Math.round(Math.random() * (2286 - 1) + 1);
        const url = `https://cors-anywhere.herokuapp.com/https://xkcd.com/${random}/info.0.json`
        const axios = require('axios');

        // Request to url
        axios.get(url)
            .then(function (response) {
                if (response) {
                    // Set of data
                    setComic(response.data)
                    // Set of loading component
                    setLoading(false)
                }
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    return (
        <div className="content">
            {loading ? <CircularProgress />
                :
                <div className="content-comic">
                    <p className="content-comic-title">{Comic.title}</p> <img src={Comic.img} alt="comic-pic" ></img>
                </div>
            }

            <div className="content-stars">
                <Box component="fieldset" mb={3} borderColor="transparent">
                    <p>Califica el cómic</p>
                    <Rating
                        name="simple-controlled"
                        value={value ? value : null}
                        onChange={(event, newValue) => {
                            setValue(newValue);
                        }}
                    />
                </Box>
                <Button onClick={() => getComicData()} variant="contained" color="primary" disableElevation>
                    Enviar
                </Button>
            </div>
        </div >


    )
}

export default Comic
